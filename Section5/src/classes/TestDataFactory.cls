@isTest
public class TestDataFactory  {
	
	 public static void CreateTestContact() {
		List<Account> listAcc = new List<Account>();
		for (Integer i = 0; i < 10; i++) {
			listAcc.add(new Account(Name = 'Test' + i));
		}
		insert listAcc;
	    List<Contact> listCont = new List<Contact>();
		for (Integer i = 0; i < 10; i++) {
			if(i < 5){
				listCont.add(new Contact(LastName = 'Test' + i, Account = listAcc[i], Cost__c = 10000 + 
				(Integer.valueOf(Math.random()* 10000))));
			} else {
				listCont.add(new Contact(LastName = 'Test' + i, Account = listAcc[i], Cost__c = 10000 - 
				(Integer.valueOf(Math.random()* 10000))));
			}
		}
		insert listCont;
	 }	
	
} 