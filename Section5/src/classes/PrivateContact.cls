public class PrivateContact extends Type {

    private   Integer CardNumber{get;set;} 
    private  Contact ContactRecord{get;set;}
    
    public PrivateContact(String ContactType, Contact ContactRecord, Integer CardNumber){
           this.ContactRecord = ContactRecord;
           this.CardNumber = CardNumber;
           ContactType = ContactType;
    }

    public Integer getCardNumber(){
        return CardNumber; 
    }
    public Contact getContactRecord(){
        return ContactRecord;
    }
}