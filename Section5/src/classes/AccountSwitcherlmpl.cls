public class AccountSwitcherlmpl implements AccountSwitcher {

    public  List<Type> AllContacts = new List<Type>();


    public  void getAndSort(){
        for (Contact cont : [SELECT Id, Name, Cost__c FROM Contact]) {
            if (cont.Cost__c > 10000) {
            AllContacts.add(new PrivateContact(
                RandomNumber.GetCountSameChar(RandomNumber.GetRandomNumber()),
                cont, 
                RandomNumber.GetRandomNumber()    
              ));    
            } else {
                AllContacts.add(new PublicContact(
                    'Person',
                    cont, 
                    RandomNumber.GetEvenRandomNumber()  
                ));
            }
        }
    }


    public DataResult switchAccount(){
        List<PrivateContact>  privateContact = new List<PrivateContact>();
        List<PublicContact>  publicContact = new List<PublicContact>();
        DataResult dataResult = new DataResult();
        for (Type t : AllContacts) {
            if(t instanceof PrivateContact){
               privateContact.add((PrivateContact)t); 
            } else {
                publicContact.add((PublicContact)t);
            }
        } 

        List<Contact> listCont = new List<Contact>();
         Id acc;
         Integer size = 0;
         if(privateContact.size() > publicContact.size()){
             size = publicContact.size();
         }else{
             size = privateContact.size();
         }

        for (Integer i = 0; i < size; i++) {
            if (privateContact[i].getContactType() == 'Premier') {
                dataResult.NonChangedPrivateContacts.add(privateContact[i]); }continue; 

            acc =  publicContact[i].getContactRecord().AccountId;
 
            publicContact[i].getContactRecord().AccountId = privateContact[i].getContactRecord().AccountId;

            privateContact[i].getContactRecord().AccountId = acc; 

            listCont.add(publicContact[i].getContactRecord());
  
            listCont.add(privateContact[i].getContactRecord());
             dataResult.ChangedPublicContacts.add(publicContact[i]);
             dataResult.ChangedPrivateContacts.add(privateContact[i]);
            }                
        update listCont; 
        return dataResult;
    }

    public class DataResult {

        public List<PrivateContact> ChangedPrivateContacts {get;set;}
        public List<PublicContact> ChangedPublicContacts  {get;set;}
        public List<PrivateContact> NonChangedPrivateContacts  {get;set;}

    }
}