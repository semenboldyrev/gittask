@isTest
private class TestsSection5
{
	@TestSetup
	static void setup(){

	  TestDataFactory.CreateTestContact();

	}

	 @isTest 
    public static void CreateTestContactTest() {

		Test.startTest();
		TestDataFactory.CreateTestContact();
		Test.stopTest();

		Integer num = [SELECT COUNT() FROM Contact];
		System.assertEquals(10, 10, 'kljkj');
	}

	 @isTest 
    public static void GetAndSortTest() {	 	
		AccountSwitcherlmpl instance = new AccountSwitcherlmpl();

		Test.startTest();
		instance.getAndSort();
		Test.stopTest();

		System.assert(instance.AllContacts.size() == 10); 
	}

	@isTest
	public static void switchAccountTest() {
		AccountSwitcherlmpl instance = new AccountSwitcherlmpl();

		Test.startTest();
		instance.switchAccount();
		Test.stopTest();

	    	
	}

	@isTest
	public static void getCardNumberTest() {
		Contact cont = [SELECT Name FROM Contact LIMIT 1];
		 PrivateContact prCont = new  PrivateContact(
			 'Person',		
			  cont,
			  658945
		 );
		Test.startTest();
		prCont.getContactType();
		Test.stopTest();
	}

	@isTest
	public static void getContactRecordTest() {
		Contact cont = [SELECT Name FROM Contact LIMIT 1];
		 PrivateContact prCont = new  PrivateContact(
			 'Person',		
			  cont,
			  658945
		 );
		Test.startTest();
		prCont.getContactRecord();
		Test.stopTest();
	}

	@isTest
	public static void getContactRecordTests() {
		Contact cont = [SELECT Name FROM Contact LIMIT 1];
		 PublicContact pubCont = new  PublicContact(
			 'Person',		
			  cont,
			  658945
		 );
		Test.startTest();
		pubCont.getContactRecord();
		Test.stopTest();
	}

	@isTest
	public static void getVolunteerNumberTest() {
		Contact cont = [SELECT Name FROM Contact LIMIT 1];
		 PublicContact pubCont = new  PublicContact(
			 'Person',		
			  cont,
			  658946
		 );
		Test.startTest();
		pubCont.getVolunteerNumber();
		Test.stopTest();
	}


}