public class RandomNumber {

    public static Integer randomNumber{get;set;}

    public static Integer GetRandomNumber(){
        randomNumber = Integer.valueOf(Math.random() * 1000000);
                if(randomNumber < 900000){
                  randomNumber += 100000;    
                }
    return randomNumber;
    }

    public static Integer GetEvenRandomNumber(){
        randomNumber = GetRandomNumber();
        if(randomNumber == 1){
            randomNumber -= 1;
        }
    return randomNumber;
    }

    public static String GetCountSameChar(Integer randomNumber){
        String str = String.valueOf(randomNumber);
    
        Integer counter;
        for(Integer i = 0; i < str.length() - 1; i++) { 
            String s = str.substring(i, i+1);
                counter = 0;
            for(Integer j = i + 1; j < str.length(); j++){ 
                if(s == str.substring(j, j + 1)) {
                    counter++;
                }                              
            }     
            if(counter >= 2){
                return 'Premier';
            }            
       }
    return 'Person';
    }
}