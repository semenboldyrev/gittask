public class PublicContact extends Type{
    private Integer VolunteerNumber{get;set;} 
    private Contact ContactRecord{get;set;} 

    public PublicContact(String ContactType, Contact ContactRecord, Integer VolunteerNumber){
           this.ContactRecord = ContactRecord;
           this.VolunteerNumber = VolunteerNumber;
           ContactType = ContactType;
    } 

    public Integer getVolunteerNumber(){
        return VolunteerNumber;
    }

    public Contact getContactRecord(){
        return ContactRecord;
    }
}